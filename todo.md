- [x] Done
    - [x] Melee
    - [x] Turning
    - [x] Monster templates
    - [x] Complex skills
        - [x] Multishot
        - [x] Rush
        - [x] Bomb
        - [x] Ray
- [x] Level generation
    - [x] void for doors
    - [x] exit
    - [ ] keys
    - [ ] special rooms
    - [x] fix levels
- [ ] Mechanics
    - [x] Ranged AI
    - [ ] Pathfinding
    - [x] Difficulty level
    - [x] Better bomb
- [ ] Game feel
    - [x] Correct impulse
        - [ ] also radius
        - [x] also mass
    - [x] draw hands
    - [x] Attack animation\delay
    - [ ] particles
- [ ] Progression
    - [x] Stealth
    - [ ] Tiled template
    - [x] Level exit
    - [x] Main menu
        - [x] Scores
- [ ] Refactor and fixes
    - [ ] cleanup tileset
    - [x] Pictures for skills
    - [x] fix raycasts
|

из диздока:
    + луч стреляет в первого встреченного
    +- атаки отбрасывают
    + пауза на прицеливание у стрелков

