record RemoveAtTime < ECS::Component, tick : Int32

class RemoveByTime < ECS::System
  def filter(world) : ECS::Filter?
    world.of(RemoveAtTime)
  end

  def process(ent)
    return if @world.getWorldTime.tick < ent.getRemoveAtTime.tick
    if ent.has? PhysicsBodyComponent
      ent.set(PhysicsBodyRemoveRequest.new(also_entity: true))
    else
      ent.destroy
    end
  end
end
