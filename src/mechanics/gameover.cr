record GameOver < ECS::Component

class ProcessGameOver < ECS::System
  def filter(world) : ECS::Filter?
    world.of(GameOver)
  end

  def process(entity)
    Engine.layer = 100
    center = Engine.screen_size/2
    size = v2(250, 75)
    box = aabb(center - size/2, size)
    Engine.rect(box, true, Color::WHITE)
    Engine.rect(box, false, Color::BLACK)
    fnt.draw_text_boxed("Game over", aabb(box.v0, v2(box.size.x, box.size.y/3)))
    fnt.draw_text_boxed("Press R to restart", aabb(box.v0 + v2(0, box.size.y/3), v2(box.size.x, box.size.y/3)))
    fnt.draw_text_boxed("Press Esc to show menu", aabb(box.v0 + 2*v2(0, box.size.y/3), v2(box.size.x, box.size.y/3)))
    if !Engine::Keys[Engine::Key::R].up? || !Engine::Keys[Engine::Key::G].up?
      saved = @world.getSoundSettings
      @world.delete_all
      @world.add(saved)
      @world.add(CameraPosition.new)
      @world.add(do_config_physics)
      @world.set WorldTime.new(0)
      Engine::Physics.reset
      @world.new_entity.add(NewGame.new)
    end
  end
end
