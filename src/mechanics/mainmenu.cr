record MainMenu < ECS::Component
record ShowCampaign < ECS::Component
record JustShown < ECS::Component

def start_new_game(world)
  saved = world.getSoundSettings
  world.delete_all
  world.add(CameraPosition.new)
  world.add(saved)
  world.add(do_config_physics)
  world.set WorldTime.new(0)
  Engine::Physics.reset
  world.new_entity.add(NewGame.new)
  world.query(MainMenu).each &.destroy
end

class ProcessMainMenu < ECS::System
  def execute
    Engine.layer = 100
    return unless @world.component_exists?(MainMenu)

    panel(5, 5, 68, 34, halign: HAlign::Right, valign: VAlign::Top) do
      settings = @world.getSoundSettings
      panel(1, 1, 34, 34, halign: HAlign::Flow, valign: VAlign::Top, border: Color::BLACK) do
        (settings.music ? RES::Gui::Sound_on : RES::Gui::Sound_off).draw(GUI.box.center, scale: 1/16)
        if GUI.button_state.clicked?
          @world.set(SoundSettings.new(sound: settings.sound, music: !settings.music))
          if !settings.music
            set_music(@world, RES::Determination, cfg.menu_music)
          else
            stop_music(@world)
          end
        end
      end
      panel(1, 1, 34, 34, halign: HAlign::Flow, valign: VAlign::Top, border: Color::BLACK) do
        (settings.sound ? RES::Gui::Speaker : RES::Gui::Speaker_off).draw(GUI.box.center, scale: 1/16)
        if GUI.button_state.clicked?
          @world.set(SoundSettings.new(sound: !settings.sound, music: settings.music))
        end
      end
    end

    if @world.component_exists?(ShowCampaign)
      center = Engine.screen_size/2
      size = v2(600, 500)
      box = aabb(center - size/2, size)
      panel(box.v0.x, box.v0.y, box.size.x, box.size.y, fill: Color::WHITE, border: Color::BLACK) do
        panel(0, 10, 200, 50, fill: Color::WHITE, border: Color::BLACK, halign: HAlign::Center, valign: VAlign::Flow) do
          fnt.draw_text_boxed("Назад", GUI.box, halign: HAlign::Center)
          if GUI.button_state.clicked?
            @world.query(ShowCampaign).each &.destroy
            @world.new_entity.add(JustShown.new)
          end
        end
        i = 1
        max_won = 0
        4.times do
          panel(0, 5, 580, 100, halign: HAlign::Center, valign: VAlign::Flow) do
            3.times do
              possible = i == 1 || campaign_stats[i - 1]?
              next unless possible
              panel(5, 5, 180, 90, fill: possible ? Color::WHITE : Color::SILVER, border: possible ? Color::BLACK : Color::SILVER, valign: HAlign::Center, halign: VAlign::Flow) do
                panel(0, 5, 25, 25, fill: possible ? Color::WHITE : Color::SILVER, border: possible ? Color::BLACK : Color::SILVER, valign: HAlign::Flow, halign: VAlign::Center) do
                  fnt.draw_text_boxed(i.to_s, GUI.box, halign: HAlign::Center)
                end
                if stats = campaign_stats[i]?
                  panel(5, 5, 90, 25, valign: VAlign::Flow, halign: HAlign::Left) do
                    fnt.draw_text_boxed("Лучшее время: #{sprintf("%d:%02d", stats.time // 60, stats.time % 60)}", GUI.box)
                  end
                  panel(5, 5, 90, 25, valign: VAlign::Flow, halign: HAlign::Left) do
                    fnt.draw_text_boxed("Макс. убито: #{stats.killed}%", GUI.box)
                  end
                end
                if possible && GUI.button_state.clicked? && !@world.component_exists?(JustShown)
                  cfg.seed = cfg.campaign[i - 1]
                  start_new_game(@world)
                end
              end
              i += 1
            end
          end
        end
      end
    else
      center = Engine.screen_size/2
      size = v2(250, 400)
      box = aabb(center - size/2, size)
      panel(box.v0.x, box.v0.y, box.size.x, box.size.y, fill: Color::WHITE, border: Color::BLACK) do
        if @world.component_exists?(MapData) && !@world.component_exists?(GameOver) && !@world.component_exists?(PlayerWon)
          panel(0, 10, 200, 50, fill: Color::WHITE, border: Color::BLACK, halign: HAlign::Center, valign: VAlign::Flow) do
            fnt.draw_text_boxed("Назад к игре", GUI.box, halign: HAlign::Center)
            if GUI.button_state.clicked? && !@world.component_exists?(JustShown)
              @world.query(MainMenu).each &.destroy
              @world.new_entity.add(JustShown.new)
            end
          end
        end
        panel(0, 10, 200, 50, fill: Color::WHITE, border: Color::BLACK, halign: HAlign::Center, valign: VAlign::Flow) do
          fnt.draw_text_boxed("Кампания", GUI.box, halign: HAlign::Center)
          if GUI.button_state.clicked? && !@world.component_exists?(JustShown)
            @world.new_entity.add(ShowCampaign.new)
            @world.new_entity.add(JustShown.new)
          end
        end
        panel(0, 10, 200, 50, fill: Color::WHITE, border: Color::BLACK, halign: HAlign::Center, valign: VAlign::Flow) do
          fnt.draw_text_boxed("Случайный уровень", GUI.box, halign: HAlign::Center)
          if GUI.button_state.clicked? && !@world.component_exists?(JustShown)
            cfg.seed = Random::Secure.next_int
            start_new_game(@world)
          end
        end
        panel(0, 10, 200, 50, fill: Color::WHITE, halign: HAlign::Center, valign: VAlign::Flow) do
          label("Введите код:", fnt, -10, -5, 100, 15)
          seed1 = cfg.seed.unsafe_as(Int32)
          seed2 = edit(seed1, Int32::MIN, Int32::MAX, fnt, -10, 20, 100, 30, border: Color::GREEN)
          if seed2
            cfg.seed = seed2 if seed1 != seed2
          end
          panel(110, 0, 110, 50, border: Color::BLACK) do
            fnt.draw_text_boxed("Начать", GUI.box, halign: HAlign::Center)
            if GUI.button_state.clicked? && !@world.component_exists?(JustShown)
              start_new_game(@world)
            end
          end
        end
        panel(0, 10, 200, 50, fill: Color::WHITE, border: Color::BLACK, halign: HAlign::Center, valign: VAlign::Flow) do
          fnt.draw_text_boxed("Сложность: #{["Легко", "Нормально", "Сложно"][cfg.difficulty]}", GUI.box, halign: HAlign::Center)
          if GUI.button_state.clicked? && !@world.component_exists?(JustShown)
            cfg.difficulty = (cfg.difficulty + 1) % 3
          end
          if !GUI.button_state.normal?
            hint_box = aabb(GUI.box.v0 + v2(220, 0), v2(350, 50))
            Engine.rect(hint_box, true, Color::WHITE)
            Engine.rect(hint_box, false, Color::BLACK)
            fnt.draw_text_boxed(["Урон по игроку уменьшен на 50%", "Обычный урон по игроку", "Теряем #{(cfg.hard_damage*100).to_i}% макс. здоровья каждую секунду"][cfg.difficulty], hint_box, halign: HAlign::Center)
          end
        end
        panel(0, 10, 200, 50, fill: Color::WHITE, border: Color::BLACK, halign: HAlign::Center, valign: VAlign::Flow) do
          fnt.draw_text_boxed("Выход", GUI.box, halign: HAlign::Center)
          if GUI.button_state.clicked? && !@world.component_exists?(JustShown)
            @world.new_entity.add(QuitEvent.new)
          end
        end
      end
    end
    @world.query(JustShown).each &.destroy
  end
end
