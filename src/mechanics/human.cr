record UnitPicture < ECS::Component, typ : MonsterType
record AttackAnimation < ECS::Component, angle1 : Float64, angle2 : Float64, time : Int32
record ExecutingAnimation < ECS::Component, start : Int32, source : ECS::Entity

class RenderUnits < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([UnitPicture, RenderPosition])
  end

  @aabb : AABB = aabb(v2(0, 0), v2(1000, 1000))

  def preprocess
    camera = @world.getCameraPosition.offset
    aabb = aabb(camera, Engine.screen_size)
    @aabb = aabb.cut(v2(-cfg.map_cell*2, -cfg.map_cell*2))
  end

  def process(entity)
    pos = entity.getRenderPosition.pos
    unless @aabb.includes? pos
      if anim = entity.getExecutingAnimation?
        tick = @world.getWorldTime.tick - anim.start
        script = anim.source.getAttackAnimation
        if tick > script.time
          entity.remove(ExecutingAnimation)
        end
      end
      return
    end
    angle = entity.getRenderRotation.angle
    typ = entity.getUnitPicture.typ
    color = Color::WHITE
    if entity.has? Dead
      if tim = entity.getRemoveAtTime?
        alpha = (tim.tick - @world.getWorldTime.tick) * 100 // (10*60)
        color = Engine.color(255, 100, 100, alpha)
      else
        color = Engine.color(255, 100, 100, 100)
      end
    end

    RES::Tilesheet.draw_frame(TYP_DATA[typ][:color_tile].to_i, pos, color: color)
    RES::Tilesheet.draw_frame(Tileset::GreenPoint.to_i, pos, color: color) if entity.has? TrackingCamera

    # a =  -angle * 180 / Math::PI - 90
    # angle * 180 / Math::PI = -a-90
    #
    hand_offset = 30

    if entity.has? UnitAiming
      hand_offset = 20
    end

    weapon_offset = 10

    sight = Vector2.from_angle(-angle/180*Math::PI - Math::PI/2, 1.0)

    if anim = entity.getExecutingAnimation?
      tick = @world.getWorldTime.tick - anim.start
      script = anim.source.getAttackAnimation
      a1 = script.angle1 / 180 * Math::PI
      a2 = script.angle2 / 180 * Math::PI
      hand_angle = a1 + (a2 - a1)*tick/script.time

      RES::Tilesheet.draw_frame(TYP_DATA[typ][:color_tile].to_i + 1, pos + sight.rotate(hand_angle)*hand_offset, color: color)
      RES::Tilesheet.draw_frame TYP_DATA[typ][:tile].to_i, pos + sight.rotate(hand_angle)*hand_offset + sight*weapon_offset, angle: -(sight.angle + hand_angle)/Math::PI*180 - 90, color: color

      if tick > script.time
        entity.remove(ExecutingAnimation)
      end
    else
      RES::Tilesheet.draw_frame(TYP_DATA[typ][:color_tile].to_i + 1, pos + sight.rotate(-Math::PI/2)*hand_offset, color: color)
      RES::Tilesheet.draw_frame TYP_DATA[typ][:tile].to_i, pos + sight.rotate(-Math::PI/2)*hand_offset + sight*weapon_offset, angle: angle, color: color
    end

    RES::Tilesheet.draw_frame(TYP_DATA[typ][:color_tile].to_i + 1, pos + sight.rotate(Math::PI/2)*hand_offset, color: color)
    fnt.draw_text("zzz", pos + v2(0, -35)) if entity.has?(Sleep) && rand < 0.5
  end
end

class StartAnimations < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([Ability, AttackAnimation, FireTrigger]).exclude(Cooldown)
  end

  def process(entity)
    entity.getAbility.owner.set ExecutingAnimation.new(source: entity, start: @world.getWorldTime.tick)
  end
end
