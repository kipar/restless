record ShootAbility < ECS::Component, shot_speed : Float64, tile : Tileset
record SpiritAbility < ECS::Component
record SpiritShot < ECS::Component

record ArcherMultishot < ECS::Component, speed_delta : Float64, count : Int32, angle : Float64
record MageMultishot < ECS::Component, count : Int32, angle : Float64

record AbilityDamage < ECS::Component, min : Int32, max : Int32 do
  def generate
    (min..max).sample
  end
end

record ShotDamage < ECS::Component, value : Int32

class ProcessShooting < ECS::System
  def filter(world) : ECS::Filter?
    world.of(ShootAbility).of(FireTrigger).exclude(Cooldown)
  end

  def process(entity)
    from_player = entity.getAbility.owner.has?(TrackingCamera)
    material = if entity.has?(SpiritAbility)
                 PhysicsMaterial::SpiritArrow
               elsif from_player
                 PhysicsMaterial::PlayerShot
               else
                 PhysicsMaterial::EnemyShot
               end
    start = entity.getAbility.owner.getRenderPosition.pos
    # TODO - speed compensation
    delta = entity.getFireTrigger.direction
    delta = delta.normalize?
    start += 5*delta

    count = 1
    dangle = [0.0]
    speeds = [1.0]
    archer_multishot = entity.getArcherMultishot?
    mage_multishot = entity.getMageMultishot?
    if archer_multishot
      count = archer_multishot.count
      dangle = Array(Float64).new(count) { |i| (-archer_multishot.angle..archer_multishot.angle).sample }
      speeds = Array(Float64).new(count) { |i| 1.0 + (-archer_multishot.speed_delta..archer_multishot.speed_delta).sample }
    end
    if mage_multishot
      count = mage_multishot.count
      dangle = Array(Float64).new(count) { |i| 0.0 - mage_multishot.angle*count/2 + i*mage_multishot.angle }
      speeds = Array(Float64).new(count) { |i| 1.0 }
    end

    count.times do |i|
      shot = @world.new_entity
      shot.add(RenderTile.new(image: RES::Tilesheet, frame: entity.getShootAbility.tile.to_i, offset: v2(0, 0)))
      shot.add(RenderPosition.new(v2(0, 0)))
      shot.add(RenderRotation.new)
      shot.add(RotateBySpeed.new)
      shot.add(RenderLayer.new(layer: 3))
      shot.add(PhysicsBodyAddRequest.new(material, pos: start, speed: (delta.rotate(dangle[i]*Math::PI/180))*entity.getShootAbility.shot_speed*speeds[i]))
      shot.add(PhysicsPositionComponent.new(typ: LibEngine::PhysicCoordinatesMode::Read, x: 0, y: 0, vx: 0, vy: 0, a: 0, omega: 0))
      shot.add(SpiritShot.new) if entity.has?(SpiritAbility)
      if bomb = entity.getBomb?
        shot.add(Explosive.new(bomb.radius, entity.getAbilityDamage.generate))
        shot.add(ShotDamage.new(0))
      else
        shot.add(ShotDamage.new(entity.getAbilityDamage.generate))
      end
    end
  end
end

class ProcessShotMisses < ECS::System
  def filter(world) : ECS::Filter?
    world.of(ShotHitWall)
  end

  def process(entity)
    shot = entity.getShotHitWall.second
    return if shot.has? PhysicsBodyRemoveRequest
    shot.add(PhysicsBodyRemoveRequest.new(also_entity: true))
    make_explosion(@world, shot) if shot.has? Explosive
  end
end

record RotateBySpeed < ECS::Component

class SyncRotationWithPhysicsSpeed < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([PhysicsPositionComponent, RenderRotation, RotateBySpeed])
  end

  def process(entity)
    phys = entity.getPhysicsPositionComponent
    entity.update(RenderRotation.new(angle: (v2(phys.vy, phys.vx).angle*180/Math::PI).to_f32 + 180))
  end
end
