record Ability < ECS::Component, timeout : Int32, owner : ECS::Entity, trigger : Int32, tile : Tileset
record Cooldown < ECS::Component, started : Int32
record AbilitySound < ECS::Component, res : Engine::Sound

@[ECS::SingleFrame]
record FireTrigger < ECS::Component, direction : Vector2

@[ECS::Singleton]
record WorldTime < ECS::Component, tick : Int32

class ProcessTimeouts < ECS::System
  def init
    @world.add(WorldTime.new(0))
  end

  def execute
    @world.set(WorldTime.new(@world.getWorldTime.tick + 1))
  end

  def filter(world) : ECS::Filter?
    world.of(Cooldown)
  end

  def process(entity)
    passed = @world.getWorldTime.tick - entity.getCooldown.started
    entity.remove(Cooldown) if passed > entity.getAbility.timeout
  end
end

N_CIRCLE = 64

def show_progress(pos, r, value)
  vp = pos
  value = 1 - value
  (value * (N_CIRCLE + 1)).to_i.times do |i|
    a = -Math::PI / 2 - 2 * Math::PI / N_CIRCLE * i
    vn = pos + Vector2.from_angle(a, r)
    Engine.triangle(vp, vn, pos, 0x00000080)
    vp = vn
  end
end

class RenderAbilities < ECS::System
  def filter(world) : ECS::Filter?
    world.of(Ability)
  end

  def process(entity)
    return unless entity.getAbility.owner.has? TrackingCamera
    Engine.layer = 101
    center = v2(60, Engine.screen_size.y - 60) + entity.getAbility.trigger*v2(120, 0)
    radius = 50
    Engine.circle(center, radius, true, 0xA0A000FF)
    RES::Tilesheet.draw_frame(entity.getAbility.tile.to_i, center, scale: v2(2.0, 2.0))
    if cooldown = entity.getCooldown?
      passed = @world.getWorldTime.tick - cooldown.started
      needed = entity.getAbility.timeout
      show_progress(center, radius, passed/needed)
    end
  end
end

class StartCooldowns < ECS::System
  def filter(world) : ECS::Filter?
    world.of(Ability).of(FireTrigger).exclude(Cooldown)
  end

  def process(entity)
    if sound = entity.getAbilitySound?
      play_sound(@world, sound.res, entity.getAbility.owner.has?(TrackingCamera) ? nil : entity.getAbility.owner.getRenderPosition.pos)
    end
    entity.add(Cooldown.new(@world.getWorldTime.tick))
  end
end

class PlayerTriggerAbilities < ECS::System
  def filter(world) : ECS::Filter?
    world.of(Ability).exclude(Cooldown)
  end

  @triggered = [false, false, false]
  @q_was_up = true

  def preprocess
    @triggered[0] = Mouse.left.down?
    @triggered[1] = Mouse.right.down?
    @triggered[2] = !Engine::Keys[Engine::Key::Q].up? && @q_was_up
    @q_was_up = Engine::Keys[Engine::Key::Q].up?
  end

  def process(entity)
    return unless entity.getAbility.owner.has? TrackingCamera
    return if entity.getAbility.owner.has? Dead
    return unless @triggered[entity.getAbility.trigger]
    entity.add(FireTrigger.new(Mouse.pos - Engine.screen_size/2))
  end
end
