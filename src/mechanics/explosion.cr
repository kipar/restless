# component on ability
record Bomb < ECS::Component, radius : Float64
# component on shot, also present on explosion
record Explosive < ECS::Component, radius : Float64, damage : Int32
# final explosion
record Explosion < ECS::Component, tick : Int32
record ExplosionDamagePending < ECS::Component

N_EXPL    = 20
EXPL_TIME = 10

def make_explosion(world, source)
  # explosion = world.new_entity
  # explosion.add RenderPosition.new(shot.getRenderPosition.pos)
  # explosion.add shot.getExplosive
  # explosion.add Explosion.new(world.getWorldTime.tick)
  # explosion.add ExplosionDamagePending.new
  # explosion.add RemoveAtTime.new(world.getWorldTime.tick + EXPL_TIME)
  speed_val = source.getExplosive.radius / EXPL_TIME*60
  play_sound(world, RES::Skills::Explode, source.getRenderPosition.pos)
  N_EXPL.times do
    shot = world.new_entity
    shot.add(RenderTile.new(image: RES::Tilesheet, frame: Tileset::RedPoint.to_i, offset: v2(0, 0)))
    shot.add(RenderPosition.new(v2(0, 0)))
    shot.add(RenderLayer.new(layer: 3))
    speed = v2(rand - 0.5, rand - 0.5).normalize? * speed_val
    if body = source.getPhysicsBodyComponent?
      material = body.typ
    else
      material = source.getPhysicsBodyAddRequest.typ
    end
    shot.add(PhysicsBodyAddRequest.new(material, pos: source.getRenderPosition.pos + speed.normalize, speed: speed))
    shot.add(PhysicsPositionComponent.new(typ: LibEngine::PhysicCoordinatesMode::Read, x: 0, y: 0, vx: 0, vy: 0, a: 0, omega: 0))
    shot.add(ShotDamage.new({source.getExplosive.damage * 2 // N_EXPL, 1}.max))
    shot.add RemoveAtTime.new(world.getWorldTime.tick + EXPL_TIME)
  end

  # explosion
end

class ApplyExplosionDamage < ECS::System
  def filter(world) : ECS::Filter?
    world.of(ExplosionDamagePending)
  end

  def process(entity)
    tick = @world.getWorldTime.tick - entity.getExplosion.tick
    pos = entity.getRenderPosition.pos
    radius = entity.getExplosive.radius
    radius2 = radius*radius
    entity.remove(ExplosionDamagePending)
    @world.query(MaxHealth).each do |target|
      next unless (target.getRenderPosition.pos - pos).sqrlength < radius2
      target.add(TakeHit.new(entity.getExplosive.damage, false))
    end
  end
end

class RenderExplosions < ECS::System
  def filter(world) : ECS::Filter?
    world.of(Explosion)
  end

  def preprocess
    Engine.layer = 5
  end

  def process(entity)
    pos = entity.getRenderPosition.pos
    data = entity.getExplosive
    tick = @world.getWorldTime.tick - entity.getExplosion.tick
    Engine.circle(pos, data.radius * tick / EXPL_TIME, true, Color::RED, Color::WHITE)
  end
end
