@[ECS::Singleton]
record SoundSettings < ECS::Component, music : Bool, sound : Bool

@[ECS::Singleton]
record Music < ECS::Component, res : Engine::Sound

def set_music(world, res : Engine::Sound, volume)
  return unless world.getSoundSettings.music
  if now = world.getMusic?
    return if now.res == res
  end
  world.set(Music.new(res))
  res.music(volume)
end

def stop_music(world)
  return unless world.component_exists? Music
  world.remove(Music)
  NO_MUSIC.music(0)
end

def play_sound(world, res, pos)
  return unless world.getSoundSettings.sound
  volume = 0
  if !pos
    volume = 100
  else
    distance = (pos - world.query(TrackingCamera).first.not_nil!.getRenderPosition.pos).length/cfg.map_cell
    volume = cfg.enemy_sound*(1 - distance/cfg.sound_distance)
    volume = cfg.enemy_sound if volume > cfg.enemy_sound
    volume = cfg.min_sound if volume < cfg.min_sound
  end
  if volume > 0
    res.play(Random::DEFAULT.next_u.unsafe_as(Pointer(Void)), volume.to_i)
  end
end
