GAME_VERSION = 4

@[ECS::Singleton]
record InitialMonsters < ECS::Component, value : Int32

record LevelStatistics, time : Int32, killed : Int32 do
  include YAML::Serializable
end

class GameStatistics
  property data = {} of Int32 => LevelStatistics
  property checksum = 0i64
  include YAML::Serializable

  @@instance : GameStatistics?

  def self.instance
    @@instance.not_nil!
  end

  def initialize
  end

  def self.load
    if File.exists?("./stats")
      File.open("./stats", "r") { |f| @@instance = GameStatistics.from_yaml(f) }
      hasher = Crystal::Hasher.new(1, 1)
      if instance.checksum != {instance.data, RES::Config.as_string, GAME_VERSION}.hash(hasher).result.unsafe_as(Int64)
        instance.data.clear
      end
      instance.checksum = 0i64
    else
      @@instance = self.new
    end
  end

  def save
    hasher = Crystal::Hasher.new(1, 1)
    RES::Config.as_string.hash(hasher)
    GAME_VERSION.hash(hasher)
    @checksum = {@data, RES::Config.as_string, GAME_VERSION}.hash(hasher).result.unsafe_as(Int64)

    File.open("./stats", "w") { |f| GameStatistics.instance.to_yaml(f) }
  end
end

def campaign_stats
  GameStatistics.instance.data
end

class ProcessVictory < ECS::System
  def filter(world) : ECS::Filter?
    world.of(PlayerWon)
  end

  def process(entity)
    camp_level = cfg.campaign.index(cfg.seed)
    if camp_level
      now = @world.query(HasAI).count { |x| !x.has?(Dead) }
      was = @world.getInitialMonsters.value
      was += 1 if was == 0
      percent = (was - now)*100//was
      percent = 99 if now > 0 && percent >= 100
      time = @world.getWorldTime.tick // 60
      if old_stats = campaign_stats[camp_level + 1]?
        time = {time, old_stats.time}.min
        percent = {percent, old_stats.killed}.max
      end
      campaign_stats[camp_level + 1] = LevelStatistics.new(time, percent)
      GameStatistics.instance.save

      if camp_level == cfg.campaign.size - 1
        @world.new_entity.add(MainMenu.new)
        @world.new_entity.add(ShowCampaign.new)
      else
        cfg.seed = cfg.campaign[camp_level + 1]
        start_new_game(@world)
      end
    else
      @world.new_entity.add(MainMenu.new)
    end
  end
end
