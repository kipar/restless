record Health < ECS::Component, value : Int32
record MaxHealth < ECS::Component, value : Int32

@[ECS::Multiple]
@[ECS::SingleFrame]
record TakeHit < ECS::Component, value : Int32, spiritual : Bool = false

record Dead < ECS::Component

# @[ECS::SingleFrame]
# record RestlessSwap < ECS::Component

class RenderHealth < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([Health, MaxHealth, RenderPosition])
  end

  def process(entity)
    percent = entity.getHealth.value / entity.getMaxHealth.value
    pos = entity.getRenderPosition.pos
    dy = -30
    width = 50
    height = 5
    color = Engine::Color::RED
    bar = aabb(pos + v2(-width/2, dy), v2(width*percent, height))
    Engine.layer = 3
    Engine.rect(bar, true, color)
  end
end

class ProcessShotHitsEnemy < ECS::System
  def filter(world) : ECS::Filter?
    # world.any_of([ShotHitsEnemy, ShotHitsPlayer]) don't work due to Crystal bug
    world.of(ShotHitsEnemy)
  end

  @spirit_mode = false

  def process(entity)
    if entity.has?(ShotHitsEnemy)
      shot = entity.getShotHitsEnemy.second
      target = entity.getShotHitsEnemy.first
    elsif entity.has?(ShotHitsPlayer)
      shot = entity.getShotHitsPlayer.second
      target = entity.getShotHitsPlayer.first
    elsif entity.has?(SpiritShotHitsEnemy)
      shot = entity.getSpiritShotHitsEnemy.second
      target = entity.getSpiritShotHitsEnemy.first
    else
      raise "BUG: incorrect event"
    end
    return if shot.has? PhysicsBodyRemoveRequest
    shot.add(PhysicsBodyRemoveRequest.new(also_entity: true))
    return unless shot.has? ShotDamage

    if shot.has? Explosive
      make_explosion(@world, shot)
    else
      target.add(TakeHit.new(shot.getShotDamage.value, @spirit_mode))
    end
  end

  def execute
    @world.query(ShotHitsPlayer).each do |entity|
      process(entity)
    end
    @spirit_mode = true
    @world.query(SpiritShotHitsEnemy).each do |entity|
      process(entity)
    end
    @spirit_mode = false
  end
end

class ProcessDamageDeath < ECS::System
  def filter(world) : ECS::Filter?
    world.of(TakeHit).any_of([Health, MaxHealth]).exclude(Dead)
  end

  def process(entity)
    hp = entity.has?(Health) ? entity.getHealth.value : entity.getMaxHealth.value
    entity.remove_if_present(Sleep)
    return if hp <= 0
    damage = entity.getTakeHit.value

    if apos = entity.getRenderPosition
      bub = @world.new_entity
      bub.add(RemoveAtTime.new(@world.getWorldTime.tick + 60))
      bub.add(DamageBubble.new("-#{damage}", apos.pos + v2(0 + rand*10, -30 - rand*10)))
    end

    if cfg.difficulty == 0 && entity.has?(TrackingCamera)
      damage = (damage*cfg.easy_bonus).to_i
    end
    hp -= damage
    if hp <= 0
      turn_into_body(@world, entity)
      if entity.getTakeHit.spiritual
        turn_into_player(@world, entity)
      end
    else
      entity.set(Health.new(hp))
      entity.remove_if_present Sleep
    end
  end
end

class ProcessBodyTurn < ECS::System
  def filter(world) : ECS::Filter?
    world.of(SpiritShotHitsBody)
  end

  def process(entity)
    shot = entity.getSpiritShotHitsBody.second
    return if shot.has? PhysicsBodyRemoveRequest
    shot.add(PhysicsBodyRemoveRequest.new(also_entity: true))
    target = entity.getSpiritShotHitsBody.first
    turn_into_player(@world, target)
  end
end

@[ECS::Singleton]
record PrevHit < ECS::Component, value : Int32

class ProcessHardDamage < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([TrackingCamera, MaxHealth])
  end

  def process(entity)
    return unless cfg.difficulty == 2
    prev_hit = @world.component_exists?(PrevHit) ? @world.getPrevHit.value : 0
    return if @world.getWorldTime.tick < prev_hit + 60
    @world.set(PrevHit.new(@world.getWorldTime.tick))
    entity.add(TakeHit.new((entity.getMaxHealth.value*cfg.hard_damage).to_i))
  end
end

record DamageBubble < ECS::Component, text : String, pos : Vector2

class RenderDamageBubbles < ECS::System
  def filter(world) : ECS::Filter?
    world.of(DamageBubble)
  end

  def process(entity)
    bub = entity.getDamageBubble
    Engine.layer = 6
    fnt.draw_text(bub.text, bub.pos)
    entity.update(DamageBubble.new(bub.text, bub.pos + v2(rand*0.1, -rand)))
  end
end
