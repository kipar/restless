record MeleeStrike < ECS::Component, range : Float64, angle : Float64

class ProcessMelee < ECS::System
  def filter(world) : ECS::Filter?
    world.of(MeleeStrike).of(FireTrigger).exclude(Cooldown)
  end

  def process(entity)
    from_player = entity.getAbility.owner.has?(TrackingCamera)
    start = entity.getAbility.owner.getRenderPosition.pos
    delta = entity.getFireTrigger.direction
    if from_player
      targets = @world.query(HasAI)
    else
      targets = @world.query(TrackingCamera)
    end
    melee = entity.getMeleeStrike
    targets.each do |target|
      next if target.has? Dead
      pos = target.getRenderPosition.pos
      delta2 = (pos - start)
      next if delta2.sqrlength > melee.range*melee.range
      if delta == delta2
        angle = 0
      else
        angle = Math.acos(delta.dot(delta2) / delta.length / delta2.length)
      end
      next if angle.abs > melee.angle/2 * Math::PI / 180
      target.add(TakeHit.new(entity.getAbilityDamage.generate, false))
      delta = delta2.normalize? * cfg.melee_force
      target.add(PhysicsApplyForceRequest.new(fx: delta.x, fy: delta.y))
    end
  end
end
