enum AIMode
  ToPlayer
  CW
  FromPlayer
  CCW
end

record HasAI < ECS::Component, mode : AIMode = AIMode::ToPlayer, switch : Int32 = 0
record AnyMelee < ECS::Component
record AnyRanged < ECS::Component

record UnitAiming < ECS::Component, finish : Int32
record AbilityAiming < ECS::Component, finish : Int32

record Sleep < ECS::Component
@[ECS::SingleFrame]
record SeePlayer < ECS::Component

record MovingOnPath < ECS::Component

class ProcessAI < ECS::System
  def filter(world) : ECS::Filter?
    world.of(HasAI).exclude(Rushing).exclude(UnitAiming)
  end

  def process(entity)
    pos = entity.getRenderPosition.pos
    player_pos = @world.query(TrackingCamera).first.not_nil!.getRenderPosition.pos
    target = player_pos

    if @world.getWorldTime.tick > 2
      if tuple = Engine.raycast_first(pos, target, PhysicsMaterial::SeePlayer)
        if ECS::Entity.new(@world, tuple[0].unsafe_as(ECS::EntityID)).has? TrackingCamera
          entity.add(SeePlayer.new)
          entity.remove_if_present(Sleep)
        end
      end
    end
    return if entity.has? Sleep

    stuck = entity.getPhysicsPositionComponent.vx.abs < 1e-2 && entity.getPhysicsPositionComponent.vy.abs < 1e-2 && (player_pos - pos).sqrlength > 64*64

    if cfg.pathfinding && (!entity.has?(SeePlayer) || stuck || entity.has?(MovingOnPath))
      callback = ->(fromx : Int32, fromy : Int32, tox : Int32, toy : Int32, data : Void*) do
        world = data.as(ECS::World)
        # puts "#{fromx}, #{fromy} -> #{tox}, #{toy}"
        # inp[fromx*cfg.map_width + fromy].passable? ? 1.0f32 : -1.0f32
        world.getMapData.walls[fromx, fromy].passable? ? 1.0f32 : -1.0f32
      end
      from_pos = (pos / cfg.map_cell)
      to_pos = (target / cfg.map_cell)
      if LibEngine.pathfind(cfg.map_width, cfg.map_height, LibEngine::PathfindAlgorithm::DijkstraReuse, -1, from_pos.x.floor.to_i, from_pos.y.floor.to_i, to_pos.x.floor.to_i, to_pos.y.floor.to_i, out ax, out ay, callback, @world.as(Void*))
        target = v2(ax * cfg.map_cell + cfg.map_cell/2, ay * cfg.map_cell + cfg.map_cell/2)
        entity.set(MovingOnPath.new)
      end
    end

    leash = (target - pos).normalize? * cfg.move_target + pos

    should_change = @world.getWorldTime.tick > entity.getHasAI.switch + 2*60

    if entity.getHasAI.mode.to_player? && !entity.has? AnyMelee
      should_change |= (player_pos - pos).sqrlength < (3*64)*(3*64)
    end

    if !entity.getHasAI.mode.to_player? && !entity.has? AnyRanged
      should_change |= rand < 0.1
    end

    if should_change
      entity.remove_if_present MovingOnPath
      new_value = if entity.has?(SeePlayer)
                    if stuck
                      AIMode.values.sample
                    else
                      AIMode::ToPlayer
                    end
                  else
                    if entity.has?(AnyRanged) || stuck
                      AIMode.values.sample
                    else
                      AIMode::ToPlayer
                    end
                  end
      # p "vx #{entity.getPhysicsPositionComponent.vx}  vy #{entity.getPhysicsPositionComponent.vy}" if !entity.has?(AnyRanged)
      # p "see#{entity.has?(SeePlayer)}, stuck#{stuck} , switch:#{new_value}" if !entity.has?(AnyRanged)
      entity.update(HasAI.new(mode: new_value, switch: @world.getWorldTime.tick))
    end

    leash = leash.rotate(Math::PI/2 * entity.getHasAI.mode.to_i) # if entity.has?(SeePlayer) || (player_pos - pos).sqrlength < (3*64)*(3*64)

    ctrl = entity.getDefaultControl
    entity.update(PhysicsApplyControl.new(leash.x, leash.y, max_speed: ctrl.max_speed, max_force: ctrl.max_force))
  end
end

class AITriggerAbilities < ECS::System
  def filter(world) : ECS::Filter?
    world.of(Ability).exclude(Cooldown).exclude(AbilityAiming)
  end

  def process(entity)
    return unless entity.getAbility.owner.has? HasAI
    return if entity.getAbility.owner.has? UnitAiming
    return if entity.getAbility.owner.has? Dead
    return if entity.has? Cooldown
    start = entity.getAbility.owner.getRenderPosition.pos
    target = @world.query(TrackingCamera).first.not_nil!.getRenderPosition.pos
    delta = target - start
    aim_time = 6

    if entity.has? MeleeStrike
      aim_time = 6
      return unless delta.length < entity.getMeleeStrike.range
    end
    if entity.has?(ShootAbility) || entity.has?(Ray)
      aim_time = 24
      return unless entity.getAbility.owner.has? SeePlayer
    end
    if entity.has? Rush
      aim_time = 12
      return unless delta.length < entity.getRush.range && entity.getAbility.owner.has? SeePlayer
    end

    tick = @world.getWorldTime.tick + aim_time
    entity.add(AbilityAiming.new(tick))
    entity.getAbility.owner.add(UnitAiming.new(tick))
  end
end

class FinishAiming1 < ECS::System
  def filter(world) : ECS::Filter?
    world.of(AbilityAiming)
  end

  def process(entity)
    if entity.getAbilityAiming.finish <= @world.getWorldTime.tick
      entity.remove AbilityAiming
      start = entity.getAbility.owner.getRenderPosition.pos
      target = @world.query(TrackingCamera).first.not_nil!.getRenderPosition.pos
      delta = target - start
      entity.add(FireTrigger.new(delta))
    end
  end
end

class FinishAiming2 < ECS::System
  def filter(world) : ECS::Filter?
    world.of(UnitAiming)
  end

  def process(entity)
    if entity.getUnitAiming.finish <= @world.getWorldTime.tick
      entity.remove UnitAiming
    end
  end
end
