enum Tileset
  Floor  = 0
  Wall
  Void
  Grave
  Stair
  Floor2
  Floor3
  Floor4
  Floor5

  RedCircle    = 9
  RedPoint
  BlueCircle
  BluePoint
  YellowCircle
  YellowPoint
  GreenCircle
  GreenPoint
  Fireball

  Dagger      = 18
  Sword
  Longsword
  Spear1
  Spear2
  Bow
  BowSwarm
  Arrow
  SpiritArrow

  Halberd1 = 27
  Halberd2
  BigAxe
  SmallAxe
  Hammer
  Staff
  Shield1
  Shield2
  RoundHit
end
