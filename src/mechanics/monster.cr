enum MonsterType
  Infantry
  Hunter
  Brute
  Rogue
  Mage
end

TYP_DATA = {
  MonsterType::Infantry => {color_tile: Tileset::BlueCircle, tile: Tileset::Longsword, health: 70, radius: 0.75, speed: 1.5},
  MonsterType::Hunter   => {color_tile: Tileset::RedCircle, tile: Tileset::Bow, health: 50, radius: 0.9, speed: 1.0},
  MonsterType::Brute    => {color_tile: Tileset::RedCircle, tile: Tileset::BigAxe, health: 150, radius: 1.5, speed: 0.75},
  MonsterType::Rogue    => {color_tile: Tileset::BlueCircle, tile: Tileset::Dagger, health: 40, radius: 0.7, speed: 1.5},
  MonsterType::Mage     => {color_tile: Tileset::YellowCircle, tile: Tileset::Staff, health: 120, radius: 0.7, speed: 1.5},
}

record DefaultControl < ECS::Component, max_speed : Float64, max_force : Float64

def create_monster(world, typ, start, *, random_cooldown = false)
  ent = world.new_entity
  # ent.add(RenderTile.new(image: RES::Tilesheet, frame: 112, offset: v2(0, 0)))
  # ent.add(RenderTile.new(image: RES::Tilesheet, frame: TYP_DATA[typ][:tile], offset: v2(0, 0)))
  ent.add(RenderPosition.new(start))
  ent.add(RenderRotation.new)
  ent.add(RenderLayer.new(layer: 2))
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Enemy, pos: start))
  ent.add(PhysicsPositionComponent.new(typ: LibEngine::PhysicCoordinatesMode::Read, x: 0, y: 0, vx: 0, vy: 0, a: 0, omega: 0))
  ent.add(DefaultControl.new(cfg.move_speed * TYP_DATA[typ][:speed], cfg.move_force*TYP_DATA[typ][:speed]))
  ent.add(PhysicsApplyControl.new(start.x, start.y, cfg.move_speed * TYP_DATA[typ][:speed], cfg.move_force*TYP_DATA[typ][:speed]))
  ent.add(MaxHealth.new(TYP_DATA[typ][:health]))
  ent.add(HasAI.new)
  ent.add(Sleep.new)
  ent.add(UnitPicture.new(typ))

  case typ
  in .infantry?
    ent.add(AnyMelee.new)
    shot2 = world.new_entity
    shot2.add(Ability.new(timeout: 3*60, owner: ent, trigger: 1, tile: Tileset::RoundHit))
    shot2.add(MeleeStrike.new(range: 64.0, angle: 360.0))
    shot2.add(AbilityDamage.new(min: 70, max: 90))
    shot2.add(AttackAnimation.new(angle1: -90.0, angle2: -90.0 + 360 + 45, time: 24))
    shot2.add(AbilitySound.new(RES::Skills::Sword3))
    shot1 = world.new_entity
    shot1.add(Ability.new(timeout: 60, owner: ent, trigger: 0, tile: Tileset::Sword))
    shot1.add(AbilityDamage.new(min: 25, max: 50))
    shot1.add(MeleeStrike.new(range: 64.0, angle: 130.0))
    shot1.add(AttackAnimation.new(angle1: -45.0, angle2: 45.0, time: 16))
    shot1.add(AbilitySound.new(RES::Skills::Sword))
  in .hunter?
    ent.add(AnyRanged.new)
    shot2 = world.new_entity
    shot2.add(Ability.new(timeout: 10*60, owner: ent, trigger: 1, tile: Tileset::BowSwarm))
    shot2.add(AbilityDamage.new(min: 25, max: 40))
    shot2.add(ShootAbility.new(shot_speed: 700, tile: Tileset::Arrow))
    shot2.add(ArcherMultishot.new(speed_delta: 0.3, count: 5, angle: 45.0))
    shot2.add(AttackAnimation.new(angle1: -45.0, angle2: 45.0, time: 16))
    shot2.add(AbilitySound.new(RES::Skills::Crossbow3))
    shot1 = world.new_entity
    shot1.add(Ability.new(timeout: 60, owner: ent, trigger: 0, tile: Tileset::Bow))
    shot1.add(AbilityDamage.new(min: 25, max: 40))
    shot1.add(ShootAbility.new(shot_speed: 700, tile: Tileset::Arrow))
    shot1.add(AttackAnimation.new(angle1: 0.0, angle2: 0.0, time: 4))
    shot1.add(AbilitySound.new(RES::Skills::Crossbow))
  in .brute?
    ent.add(AnyMelee.new)
    shot2 = world.new_entity
    shot2.add(Ability.new(timeout: 6*60, owner: ent, trigger: 1, tile: Tileset::Halberd2))
    shot2.add(Rush.new(range: 4*64.0))
    shot2.add(AbilityDamage.new(min: 20, max: 40))
    shot2.add(AttackAnimation.new(angle1: -5.0, angle2: 5.0, time: 30))
    shot2.add(AbilitySound.new(RES::Skills::Melee3))
    shot1 = world.new_entity
    shot1.add(Ability.new(timeout: 2*60, owner: ent, trigger: 0, tile: Tileset::BigAxe))
    shot1.add(AbilityDamage.new(min: 60, max: 100))
    shot1.add(MeleeStrike.new(range: 96.0, angle: 130.0))
    shot1.add(AttackAnimation.new(angle1: -60.0, angle2: 60.0, time: 10))
    shot1.add(AbilitySound.new(RES::Skills::Melee))
  in .rogue?
    ent.add(AnyMelee.new)
    ent.add(AnyRanged.new)
    shot2 = world.new_entity
    shot2.add(Ability.new(timeout: 2*60, owner: ent, trigger: 1, tile: Tileset::RedCircle))
    shot2.add(AbilityDamage.new(min: 50, max: 100))
    shot2.add(ShootAbility.new(shot_speed: 500, tile: Tileset::BluePoint))
    shot2.add(Bomb.new(radius: 1.5*64.0))
    shot2.add(AbilitySound.new(RES::Skills::Shot_fire))
    shot1 = world.new_entity
    shot1.add(Ability.new(timeout: 60 // 4, owner: ent, trigger: 0, tile: Tileset::Dagger))
    shot1.add(AbilityDamage.new(min: 10, max: 15))
    shot1.add(MeleeStrike.new(range: 64.0, angle: 90.0))
    shot1.add(AttackAnimation.new(angle1: -30.0, angle2: 30.0, time: 4))
    shot1.add(AbilitySound.new(RES::Skills::Sword))
  in .mage?
    ent.add(AnyRanged.new)
    shot2 = world.new_entity
    shot2.add(Ability.new(timeout: 8*60, owner: ent, trigger: 1, tile: Tileset::Fireball))
    shot2.add(AbilityDamage.new(min: 20, max: 40))
    shot2.add(ShootAbility.new(shot_speed: 700, tile: Tileset::Fireball))
    shot2.add(MageMultishot.new(count: 3, angle: 10.0))
    shot2.add(AttackAnimation.new(angle1: -90.0, angle2: 90.0, time: 10))
    shot2.add(AbilitySound.new(RES::Skills::Explode))
    shot1 = world.new_entity
    shot1.add(Ability.new(timeout: 5*60, owner: ent, trigger: 0, tile: Tileset::Staff))
    shot1.add(AbilityDamage.new(min: 90, max: 150))
    shot1.add(Ray.new(delay: 2*60))
    shot1.add(AttackAnimation.new(angle1: -5.0, angle2: 5.0, time: 2*60 + 30))
    shot1.add(AbilitySound.new(RES::Skills::Curse))
  end

  shot1.add(Cooldown.new(rand(60))) if random_cooldown
  shot2.add(Cooldown.new(rand(60))) if random_cooldown

  ent
end

def turn_into_player(world, ent)
  return if ent.has? TrackingCamera
  world.query(TrackingCamera).each do |ent|
    world.query(Ability).each do |able|
      able.destroy if able.getAbility.owner == ent
    end
    ent.add(PhysicsBodyRemoveRequest.new(also_entity: true))
  end
  ent.add(TrackingCamera.new)
  ent.add(ReceiveMoveEvents.new)
  ent.remove_if_present(Dead)
  ent.remove_if_present(Health)
  ent.remove_if_present(HasAI)
  ent.remove_if_present(UnitAiming)
  ent.remove_if_present(Sleep)
  ent.remove_if_present(PhysicsBodyAddRequest)
  ent.remove_if_present(RemoveAtTime)
  ent.remove_if_present(Rushing)
  world.query(Ability).each do |able|
    if able.getAbility.owner == ent
      able.remove_if_present(RayActive)
      able.remove_if_present(AbilityAiming)
      able.remove_if_present(RayFired)
      able.remove_if_present(Cooldown)
    end
  end
  world.query(GameOver).each &.remove(GameOver)

  ent.update(MaxHealth.new((ent.getMaxHealth.value * cfg.balance_hp).to_i))

  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Player, pos: ent.getRenderPosition.pos))
  if shot3 = world.query(SpiritAbility).first?
    old = shot3.getAbility
    shot3.set(Ability.new(timeout: old.timeout, owner: ent, trigger: old.trigger, tile: old.tile))
  else
    shot3 = world.new_entity
    shot3.add(Ability.new(timeout: 60, owner: ent, trigger: 2, tile: Tileset::SpiritArrow))
    shot3.add(ShootAbility.new(shot_speed: 700.0, tile: Tileset::SpiritArrow))
    shot3.add(AbilityDamage.new(min: 2, max: 5))
    shot3.add(SpiritAbility.new)
  end

  world.query(SpiritShot).each &.set(PhysicsBodyRemoveRequest.new(also_entity: true))
  play_sound(world, RES::Skills::Warp, nil)
end

def turn_into_body(world, ent)
  ent.add(Dead.new)
  ent.remove_if_present(Health)
  ent.remove_if_present(HasAI)
  ent.remove_if_present(Sleep)
  ent.remove_if_present(Rushing)
  ent.remove_if_present(ExecutingAnimation)
  ent.remove_if_present(UnitAiming)
  world.query(Ability).each do |able|
    if able.getAbility.owner == ent
      able.remove_if_present(AbilityAiming)
      able.remove_if_present(RayActive)
      able.remove_if_present(RayFired)
      able.remove_if_present(Cooldown)
    end
  end
  ent.add RemoveAtTime.new(world.getWorldTime.tick + 10*60) unless ent.has? TrackingCamera
  ent.remove_if_present ReceiveMoveEvents
  if ent.has? TrackingCamera
    world.new_entity.add(GameOver.new)
  end
  phys = ent.getPhysicsPositionComponent
  ctrl = ent.getDefaultControl
  ent.set(PhysicsBodyAddRequest.new(PhysicsMaterial::Body, pos: v2(phys.x, phys.y)))
  ent.update(PhysicsApplyControl.new(phys.x, phys.y, ctrl.max_speed, ctrl.max_force))
end
