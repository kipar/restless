record Rush < ECS::Component, range : Float64
record Rushing < ECS::Component, finish : Int32, damage : Int32

class ProcessBumps < ECS::System
  def filter(world) : ECS::Filter?
    world.of(PlayerBumpEnemy)
  end

  def process(entity)
    player = entity.getPlayerBumpEnemy.first
    monster = entity.getPlayerBumpEnemy.second

    if rush = player.getRushing?
      monster.add(TakeHit.new(rush.damage))
    end
    if rush = monster.getRushing?
      player.add(TakeHit.new(rush.damage))
    end
  end
end

class RemoveRushes < ECS::System
  def filter(world) : ECS::Filter?
    world.of(Rushing)
  end

  def process(entity)
    if @world.getWorldTime.tick >= entity.getRushing.finish
      entity.remove Rushing
    end
  end
end

class StartRush < ECS::System
  def filter(world) : ECS::Filter?
    world.of(Rush).of(FireTrigger).exclude(Cooldown)
  end

  def process(entity)
    owner = entity.getAbility.owner
    start = owner.getRenderPosition.pos
    target = start + entity.getFireTrigger.direction.normalize? * entity.getRush.range
    ctrl = owner.getDefaultControl
    owner.update(PhysicsApplyControl.new(target.x, target.y, max_speed: ctrl.max_speed*10, max_force: ctrl.max_force*10))
    owner.set(Rushing.new(damage: entity.getAbilityDamage.generate, finish: @world.getWorldTime.tick + 30))
  end
end
