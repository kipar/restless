record Ray < ECS::Component, delay : Int32
record RayActive < ECS::Component, start : Int32, stop : Int32
record RayFired < ECS::Component

class ProcessRays < ECS::System
  def filter(world) : ECS::Filter?
    world.of(RayActive)
  end

  def process(entity)
    Engine.layer = 6
    start = entity.getAbility.owner.getRenderPosition.pos
    if entity.getAbility.owner.has? TrackingCamera
      target = (Mouse.pos - Engine.screen_size/2).normalize? * 10000 + start
      mat = PhysicsMaterial::SeeEnemy
    else
      target = @world.query(TrackingCamera).first.not_nil!.getRenderPosition.pos
      mat = PhysicsMaterial::SeePlayer
    end
    if tuple = Engine.raycast_first(start, target, mat)
      # target2 = Engine.raycast_first(target, start, PhysicsMaterial::SeePlayer).not_nil![1] if
      target = tuple[1]
      under_attack = ECS::Entity.new(@world, tuple[0].unsafe_as(ECS::EntityID))
      under_attack = nil unless under_attack.has?(MaxHealth)
    else
      under_attack = nil
    end

    if @world.getWorldTime.tick > entity.getRayActive.stop
      if entity.has?(RayFired)
        entity.remove(RayActive)
        entity.remove(RayFired)
      else
        # deal damage
        if under_attack
          under_attack.add(TakeHit.new(entity.getAbilityDamage.generate))
        end
        entity.add(RayFired.new)
        entity.set(RayActive.new(@world.getWorldTime.tick, @world.getWorldTime.tick + 20))
        play_sound(@world, RES::Skills::Traphit, entity.getAbility.owner.has?(TrackingCamera) ? nil : target)
      end
    end

    Engine.line_settings(5) if entity.has?(RayFired)
    Engine.line(start, target, Color::YELLOW)
    Engine.line_settings(1)

    # Engine.circle(target, 10, false, Color::GREEN)
  end
end

class ShootRays < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([Ray, FireTrigger]).exclude([RayActive, Cooldown])
  end

  def process(entity)
    entity.add(RayActive.new(@world.getWorldTime.tick, @world.getWorldTime.tick + entity.getRay.delay))
  end
end
