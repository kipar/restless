class SyncPositionWithPhysicsSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(PhysicsPositionComponent).any_of([RenderPosition, RenderRotation])
  end

  def process(entity)
    phys = entity.getPhysicsPositionComponent
    entity.update(RenderPosition.new(v2(phys.x, phys.y))) if entity.has? RenderPosition
    # entity.update(RenderRotation.new(angle: phys.a.to_f32)) if entity.has? RenderRotation
  end
end

record TrackingCamera < ECS::Component

class TrackCameraSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(TrackingCamera).any_of([RenderPosition, RenderRotation])
  end

  def process(entity)
    offset = if pos = entity.getRenderPosition?
               pos.pos - Engine.screen_size/2
             else
               v2(0, 0)
             end
    # angle = if rot = entity.getRenderRotation?
    #           rot.angle
    #         else
    #           0.0f32
    #         end
    angle = 0.0f32
    camera = entity.getCameraPosition
    # TODO - smooth camera?
    entity.update(CameraPosition.new(offset, v2(1, 1), angle))
  end
end

record ReceiveMoveEvents < ECS::Component

class MoveByKeyboard < ECS::System
  def filter(world) : ECS::Filter?
    world.of(ReceiveMoveEvents).exclude(Rushing)
  end

  def process(entity)
    phys = entity.getPhysicsPositionComponent
    target = v2(0, 0)
    step = cfg.move_target
    if !Engine::Keys[Engine::Key::Up].up? || !Engine::Keys[Engine::Key::W].up?
      target += v2(0, -1)
    end
    if !Engine::Keys[Engine::Key::Down].up? || !Engine::Keys[Engine::Key::S].up?
      target += v2(0, 1)
    end
    if !Engine::Keys[Engine::Key::Left].up? || !Engine::Keys[Engine::Key::A].up?
      target += v2(-1, 0)
    end
    if !Engine::Keys[Engine::Key::Right].up? || !Engine::Keys[Engine::Key::D].up?
      target += v2(1, 0)
    end
    target = step*target.normalize? + v2(phys.x, phys.y)

    ctrl = entity.getDefaultControl
    entity.update(PhysicsApplyControl.new(target.x, target.y, max_speed: ctrl.max_speed, max_force: ctrl.max_force))
  end
end

class RotateByMouse < ECS::System
  def filter(world) : ECS::Filter?
    world.of(ReceiveMoveEvents)
  end

  def process(entity)
    delta = Mouse.pos - Engine.screen_size/2
    angle = -delta.angle * 180 / Math::PI - 90
    entity.update(RenderRotation.new(angle: angle.to_f32)) if entity.has? RenderRotation
  end
end

class RotateToPlayer < ECS::System
  def filter(world) : ECS::Filter?
    world.of(HasAI).exclude(Sleep).exclude(Dead)
  end

  def process(entity)
    delta = @world.query(TrackingCamera).first.not_nil!.getRenderPosition.pos - entity.getRenderPosition.pos
    angle = -delta.angle * 180 / Math::PI - 90
    entity.update(RenderRotation.new(angle: angle.to_f32)) if entity.has? RenderRotation
  end
end
