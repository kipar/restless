require "../bearlib/mapgen"

enum LevelTile
  Empty
  Wall
  Void
  Exit
  Start

  def passable?
    !(self.wall? || self.void?)
  end
end

PICS = {
  LevelTile::Empty => Tileset::Floor,
  LevelTile::Wall  => Tileset::Wall,
  LevelTile::Void  => Tileset::Void,
  LevelTile::Exit  => Tileset::Stair,
  LevelTile::Start => Tileset::Grave,
}

@[ECS::Singleton]
record MapData < ECS::Component, walls : Array2D(LevelTile), rooms_data : BearMG::Rooms, shown : Array2D(Tileset)
@[ECS::SingleFrame]
record NewGame < ECS::Component

def check_map(input, start)
  return true if cfg.seed > 0 && cfg.seed <= 13 # hack for campaign levels
  callback = ->(fromx : Int32, fromy : Int32, tox : Int32, toy : Int32, map : Void*) do
    inp = map.as(Array(BearLibMG::CellType))
    inp[fromx*cfg.map_width + fromy].passable? ? 1.0f32 : -1.0f32
  end
  LibEngine.pathfind(cfg.map_width, cfg.map_height, LibEngine::PathfindAlgorithm::DijkstraNew, -1, start.x, start.y, start.x, start.y, out ax, out ay, callback, input.as(Void*))
  return !LibEngine.path_find_unreachable(out ax2, out ay2)
end

class CreateField < ECS::System
  def filter(world) : ECS::Filter?
    world.of(NewGame)
  end

  def process(entity)
    # level generation
    seed = cfg.seed
    rooms = BearMG::Rooms.new
    input = BearMG.generate_map(cfg.map_width, BearLibMG::MapGenerator::G_NICE_DUNGEON, seed, rooms)
    rooms.acquire_data
    loop do
      break if rooms.rooms.size > 1 && rooms.rooms[0].links.size > 0 && check_map(input, rooms.rooms[0].pos + rooms.rooms[0].size//2)
      seed += 1
      Engine.log "seed increased to #{seed}"
      rooms = BearMG::Rooms.new
      input = BearMG.generate_map(cfg.map_width, BearLibMG::MapGenerator::G_NICE_DUNGEON, seed, rooms)
      rooms.acquire_data
    end
    rng = Random::PCG32.new(cfg.seed.to_i64.unsafe_as(UInt64))
    walls = Array2D(LevelTile).new(v2i(cfg.map_width, cfg.map_height)) do |v|
      # LevelTile.new(rand(3))
      if input[v.y*cfg.map_width + v.x].passable?
        LevelTile::Empty
      else
        LevelTile::Wall
      end
    end
    walls = walls.map_with_index do |v, pos|
      if v.wall? && walls.count_neightboors(pos, &.empty?) >= 3 && rng.rand < cfg.void_start
        LevelTile::Void
      else
        v
      end
    end

    cfg.void_passes.times do
      walls = walls.map_with_index do |v, pos|
        if v.wall? && rng.rand < cfg.void_expansion*walls.count_neightboors(pos, &.void?)
          LevelTile::Void
        else
          v
        end
      end
    end
    start = rooms.rooms[0].pos + rooms.rooms[0].size//2
    walls[start] = LevelTile::Start
    # walls[rooms.rooms[0].pos] = LevelTile::Exit

    far = rooms.rooms.max_by do |r|
      center = r.pos + r.size//2
      v = center - start
      v.x*v.x + v.y*v.y
    end
    walls[far.pos + far.size//2] = LevelTile::Exit

    ent_wall = @world.new_entity
    ent_wall.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Wall, pos: v2(0, 0)))
    ent_void = @world.new_entity
    ent_void.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Void, pos: v2(0, 0)))
    ent_exit = @world.new_entity
    ent_exit.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Exit, pos: v2(0, 0)))

    cfg.map_width.times do |i|
      cfg.map_height.times do |j|
        box = PhysicsAddBoxShapeRequest.new(i*cfg.map_cell, j*cfg.map_cell, (i + 1)*cfg.map_cell, (j + 1)*cfg.map_cell)
        case walls[i, j]
        in .empty?
          next
        in .start?
          next
        in .wall?
          ent_wall.add(box)
        in .void?
          ent_void.add(box)
        in .exit?
          ent_exit.add(box)
        end
      end
    end

    shown = Array2D(Tileset).new(v2i(cfg.map_width, cfg.map_height)) do |v|
      PICS[walls[v]]
    end

    @world.add(MapData.new(walls, rooms, shown))
  end
end

class BetterFloor < ECS::System
  def filter(world) : ECS::Filter?
    world.of(NewGame)
  end

  def process(entity)
    rng = Random::PCG32.new(cfg.seed.to_i64.unsafe_as(UInt64))
    map = @world.getMapData.shown

    cfg.map_width.times do |i|
      cfg.map_height.times do |j|
        next unless map[i, j].floor?
        if rng.rand < 0.2
          map[i, j] = [Tileset::Floor2, Tileset::Floor3, Tileset::Floor4, Tileset::Floor5].sample(rng)
        end
      end
    end
  end
end

class InitPlayerSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(NewGame)
  end

  def process(entity)
    rng = Random::PCG32.new(cfg.seed.to_i64.unsafe_as(UInt64))
    lvl = @world.getMapData.rooms_data
    typ = MonsterType.values.sample(rng)
    if cfg.fixed_class > 0
      typ = MonsterType.new(cfg.fixed_class - 1)
    end
    ent = create_monster(@world, typ, ((lvl.rooms[0].pos + lvl.rooms[0].size//2) * cfg.map_cell).to_f)
    cfg.n_enemies.times do
      room = (1...lvl.rooms.size).sample(rng)
      room = lvl.rooms[room]
      pos = v2(rng.rand, rng.rand).mul_elem(room.size.to_f) + room.pos.to_f
      pos *= cfg.map_cell
      typ = MonsterType.values.sample(rng)
      # pos += v2(cfg.map_cell/2, cfg.map_cell/2)
      create_monster(@world, typ, pos, random_cooldown: true)
    end
    turn_into_player(@world, ent)
    @world.set(InitialMonsters.new(cfg.n_enemies))
  end
end

class RenderField < ECS::System
  def execute
    map = @world.getMapData.shown
    camera = @world.getCameraPosition.offset
    aabb = aabb(camera, Engine.screen_size)
    aabb = aabb.cut(v2(-cfg.map_cell, -cfg.map_cell))
    Engine.layer = 1
    cfg.map_width.times do |i|
      cfg.map_height.times do |j|
        pos = v2(i*cfg.map_cell + cfg.map_cell/2, j*cfg.map_cell + cfg.map_cell/2)
        next unless aabb.includes? pos
        next if map[i, j].wall? || map[i, j].grave? || map[i, j].stair?
        RES::Tilesheet.draw_frame map[i, j].to_i, pos
      end
    end
    cfg.map_width.times do |i|
      cfg.map_height.times do |j|
        pos = v2(i*cfg.map_cell + cfg.map_cell/2, j*cfg.map_cell + cfg.map_cell/2)
        next unless aabb.includes? pos
        next unless map[i, j].wall? || map[i, j].grave? || map[i, j].stair?
        RES::Tilesheet.draw_frame map[i, j].to_i, pos
      end
    end
  end
end

class InitLevelSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(NewGame)
  end

  def process(entity)
    ent = @world.new_entity
    width = cfg.map_cell*cfg.map_width
    height = cfg.map_cell*cfg.map_height
    ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Wall, v2(0, 0)))
    ent.add(PhysicsAddLineShapeRequest.new(0, 0, 0, height))
    ent.add(PhysicsAddLineShapeRequest.new(0, 0, width, 0))
    ent.add(PhysicsAddLineShapeRequest.new(0, height, width, height))
    ent.add(PhysicsAddLineShapeRequest.new(width, 0, width, height))
  end

  def execute
    LibEngine.debug_physics_render if !Engine::Keys[Engine::Key::P].up?

    if !Engine::Keys[Engine::Key::J].up?
      @world.query(Cooldown).each &.remove(Cooldown)
    end
  end
end

class RenderScore < ECS::System
  def execute
    Engine.layer = 100
    Engine.rect(aabb(x0: 1, y0: 1, w: 250, h: 75), true, Color::WHITE)
    Engine.rect(aabb(x0: 1, y0: 1, w: 250, h: 75), false, Color::BLACK)
    fnt.draw_text("Осталось врагов: #{@world.query(HasAI).size}", v2(5, 5))
    time = @world.getWorldTime.tick//60
    fnt.draw_text("Прошло времени: #{sprintf("%d:%02d", time // 60, time % 60)}", v2(5, 25))
    fnt.draw_text("Код уровня: #{cfg.seed}", v2(5, 50))
  end
end
