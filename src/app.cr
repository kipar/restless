require "./engine/engine.cr"
require "./resources.cr"
require "myecs"

require "./utils/*"
require "./game_config"
require "./game_systems"
require "./utils/physics_systems"

@[ECS::SingleFrame(check: false)]
struct QuitEvent < ECS::Component
end

{% unless flag? :spec %}
  begin
    world = ECS::World.new
    systems = ECS::Systems.new(world)
      .add(BasicSystems.new(world))
      .add(PhysicSystems.new(world))
      .add(GameSystems.new(world))
      .add(RenderSystem.new(world))


    systems.init
    GameStatistics.load
    world.new_entity.add(MainMenu.new)
    loop do
      systems.execute
      break if world.component_exists?(QuitEvent)
    end
    systems.teardown
  rescue ex : Exception
    {% if flag? :release %}
    Engine.log(ex.inspect_with_backtrace)
    Engine.log("at seed: #{cfg.seed}")
    {% else %}
    puts ex.inspect_with_backtrace
    puts "at seed: #{cfg.seed}"
    {% end %}
  end
{% end %}
ECS.debug_stats
