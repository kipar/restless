require "./bearlibmg"
require "../engine/vector2"

module BearMG
  def self.generate_map(size : Int32, typ : BearLibMG::MapGenerator, seed : Int32, rooms : Rooms?) : Array(BearLibMG::CellType)
    raw_data = Array(BearLibMG::CellType).new(size*size, BearLibMG::CellType.new(0))
    tuple = {raw_data, size}
    box = Box.box(tuple)
    BearLibMG.generate_cb(size, size, typ, seed,
      ->(x, y, cell, opaque) do
        atuple = Box(typeof(tuple)).unbox(opaque)
        atuple[0][atuple[1]*y + x] = cell
      end,
      box, nil, rooms ? rooms.raw : Pointer(Void).null)
    raw_data
  end

  class Room
    @owner : Rooms
    getter pos : IntVector2
    getter size : IntVector2
    getter link_ids : Array(Int32)
    getter index : Int32

    def get_room(index)
      @owner.rooms[link_ids[index]]
    end

    def links
      RoomLinks.new(self)
    end

    def initialize(@owner, @index)
      BearLibMG.roomsdata_position(@owner.raw, @index, out ax, out ay, out aw, out ah)
      @pos = v2i(ax, ay)
      @size = v2i(aw, ah)
      @link_ids = Array(Int32).new(BearLibMG.roomsdata_linkscount(@owner.raw, @index)) do |i|
        BearLibMG.roomsdata_getlink(@owner.raw, @index, i)
      end
    end
  end

  record RoomLinks, owner : Room do
    include Indexable(Room)

    def unsafe_fetch(index : Int) : Room
      @owner.get_room(index)
    end

    def size
      @owner.link_ids.size
    end
  end

  class Rooms
    getter raw : BearLibMG::TRoomsDataHandle
    getter rooms = [] of Room

    def initialize
      @raw = BearLibMG.roomsdata_create(BearLibMG::MapGenerator::G_NICE_DUNGEON)
    end

    def acquire_data
      @rooms = Array(Room).new(BearLibMG.roomsdata_count(@raw)) do |i|
        Room.new(self, i)
      end
      BearLibMG.roomsdata_delete(@raw)
    end
  end
end

enum BearLibMG::CellType
  def passable?
    {BearLibMG::CellType::TILE_GROUND,
     BearLibMG::CellType::TILE_WATER,
     BearLibMG::CellType::TILE_ROAD,
     BearLibMG::CellType::TILE_HOUSE_FLOOR,
     BearLibMG::CellType::TILE_GRASS,

     BearLibMG::CellType::TILE_CORRIDOR,
     BearLibMG::CellType::TILE_SMALLROOM,
     BearLibMG::CellType::TILE_BIGROOM,
     BearLibMG::CellType::TILE_DOOR,
    }.includes? self
  end
end
