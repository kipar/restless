require "./mechanics/*"

class GameSystems < ECS::Systems
  @physics_stopped = false

  def initialize(@world)
    super
    @mainmenu = ProcessMainMenu.new(@world)
    @rendered = [] of Tuple(ECS::System, ECS::Filter?)

    add KeyReactSystem.new(@world, pressed: CONFIG_PRESSED, down: CONFIG_DOWN)

    add CreateField

    add InitLevelSystem
    add InitPlayerSystem
    add BetterFloor
    remove_singleframe NewGame

    add RenderField
    @rendered << {@children.last, @children.last.filter(@world)}
    add RenderUnits
    @rendered << {@children.last, @children.last.filter(@world)}
    add RenderDamageBubbles
    @rendered << {@children.last, @children.last.filter(@world)}

    add SyncPositionWithPhysicsSystem
    add SyncRotationWithPhysicsSpeed
    add TrackCameraSystem
    add MoveByKeyboard
    add RotateByMouse
    add RotateToPlayer
    add RenderAbilities
    add ProcessTimeouts
    add FinishAiming1
    add FinishAiming2

    add PlayerTriggerAbilities
    # application of abilities
    add StartAnimations
    add ProcessShooting
    add ProcessMelee
    add ShootRays
    add StartRush
    # -------------------
    add StartCooldowns
    remove_singleframe FireTrigger

    add ProcessAI
    add AITriggerAbilities

    add ProcessRays
    @rendered << {@children.last, @children.last.filter(@world)}

    remove_singleframe SeePlayer

    # physics events
    add ProcessShotMisses
    remove_singleframe ShotHitWall
    add ProcessShotHitsEnemy
    remove_singleframe ShotHitsEnemy
    remove_singleframe ShotHitsPlayer
    remove_singleframe SpiritShotHitsEnemy
    add ProcessBodyTurn
    remove_singleframe SpiritShotHitsBody
    add ProcessBumps
    remove_singleframe PlayerBumpEnemy

    add ApplyExplosionDamage
    add RenderExplosions
    @rendered << {@children.last, @children.last.filter(@world)}

    add ProcessHardDamage

    add ProcessDamageDeath
    remove_singleframe TakeHit

    add RemoveByTime
    add RemoveRushes

    add RenderHealth
    @rendered << {@children.last, @children.last.filter(@world)}
    add RenderScore
    @rendered << {@children.last, @children.last.filter(@world)}
    add ProcessGameOver

    add ProcessVictory

    @world.add(SoundSettings.new(true, true))
  end

  def execute
    if @world.component_exists? MainMenu
      set_music(@world, RES::Determination, cfg.menu_music)
      Engine[Engine::Params::PhysicsSpeed] = 0 unless @physics_stopped
      @physics_stopped = true
      @mainmenu.execute
      if @world.component_exists? MapData
        @rendered.each do |sys, filter|
          sys.preprocess
          if filter
            filter.each { |ent| sys.process(ent) }
          end
          sys.execute
        end
      end
    else
      set_music(@world, cfg.seed.odd? ? RES::Music : RES::Music_2, cfg.battle_music)
      Engine[Engine::Params::PhysicsSpeed] = 1000 if @physics_stopped
      @physics_stopped = false
      super
    end
  end
end
