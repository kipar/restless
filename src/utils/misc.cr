class MeasureGC
  getter value = 0
  @cur_value = 0u64

  def initialize
    @time = Time.utc
    @cur_value = GC.stats.total_bytes.to_u64
    @value = 0
  end

  def value
    update
    @value
  end

  private def update
    now = Time.utc
    if now - @time > 1.seconds
      n = GC.stats.total_bytes.to_u64
      @value = (n - @cur_value).to_i
      @cur_value = n
      @time = now
    end
  end
end

def split_text(s, font, width)
  results = [] of String
  cur = ""
  s.split(' ').each do |part|
    added = "#{cur} #{part}"
    v = font.measure(added)
    if v.x > width
      results << cur.strip
      cur = part
    else
      cur = added
    end
  end
  results << cur.strip if cur != ""
  results
end

struct Number
  def degrees
    Math::PI * self / 180
  end
end

class Array2D(T)
  getter size
  getter data

  def initialize(@size : IntVector2)
    @data = Array(T).new(@size.x*@size.y)
  end

  def initialize(*, @size, @data)
  end

  def initialize(@size : IntVector2, value : T)
    @data = Array(T).new(@size.x*@size.y, value)
  end

  def initialize(@size : IntVector2, &)
    @data = Array(T).new(@size.x*@size.y) do |i|
      yield(v2i(i % size.x, i//size.x))
    end
  end

  def [](v2)
    @data[v2.x + v2.y*@size.x]
  end

  def []=(v2, value : T)
    @data[v2.x + v2.y*@size.x] = value
  end

  def [](x, y)
    @data[x + y*@size.x]
  end

  def []=(x, y, value : T)
    @data[x + y*@size.x] = value
  end

  def fill(value : T)
    @data.fill(value)
  end

  def map_with_index!(&)
    @data.map_with_index! { |v, i| yield(v, v2i(i % size.x, i//size.x)) }
  end

  def map_with_index(&)
    Array2D(T).new(@size) do |pos|
      yield(self[pos], pos)
    end
  end

  def count_neightboors(pos, diagonal = false, &)
    n = 0
    n += 1 if pos.y > 0 && yield(self[pos.x, pos.y - 1])
    n += 1 if pos.y < size.y - 1 && yield(self[pos.x, pos.y + 1])
    n += 1 if pos.x > 0 && yield(self[pos.x - 1, pos.y])
    n += 1 if pos.x < size.x - 1 && yield(self[pos.x + 1, pos.y])
    if diagonal
      n += 1 if pos.y > 0 && pos.x > 0 && yield(self[pos.x - 1, pos.y - 1])
      n += 1 if pos.y < size.y - 1 && pos.x > 0 && yield(self[pos.x - 1, pos.y + 1])
      n += 1 if pos.y > 0 && pos.x < size.x - 1 && yield(self[pos.x + 1, pos.y - 1])
      n += 1 if pos.y < size.y - 1 && pos.x < size.x - 1 && yield(self[pos.x + 1, pos.y + 1])
    end
    n
  end
end
