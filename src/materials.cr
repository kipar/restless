enum PhysicsMaterial
  Player
  Enemy
  Wall
  Void
  Exit
  Body
  PlayerShot
  SpiritArrow
  EnemyShot
  SeePlayer
  SeeEnemy
end

@[ECS::SingleFrame]
struct ShotHitsPlayer < CollisionComponent
end

@[ECS::SingleFrame]
struct ShotHitsEnemy < CollisionComponent
end

@[ECS::SingleFrame]
struct SpiritShotHitsBody < CollisionComponent
end

@[ECS::SingleFrame]
struct SpiritShotHitsEnemy < CollisionComponent
end

@[ECS::SingleFrame]
struct ShotHitWall < CollisionComponent
end

@[ECS::SingleFrame]
struct PlayerBumpEnemy < CollisionComponent
end

struct PlayerWon < CollisionComponent
end

config_physics do
  gravity(x: 0.0, y: 0.0)
  damping(cfg.damping)

  material(:wall, density: 1, friction: 0.5, elasticity: 0.5, static: true)
  material(:void, density: 1, friction: 0.5, elasticity: 0.5, static: true)

  material(:exit, density: 1, friction: 0.5, elasticity: 0.5, static: true)

  material(:player, density: 0.01, friction: 0.5, elasticity: 0.5, def_radius: 20, non_rotating: true) do
    collide :wall
    collide :void
    pass_event :exit, PlayerWon
  end

  material(:enemy, density: 0.01, friction: 0.5, elasticity: 0.5, def_radius: 20, non_rotating: true) do
    collide :wall
    collide :void
    collide_event :player, PlayerBumpEnemy
    collide :enemy
  end

  material(:body, density: 0.01, friction: 0.5, elasticity: 0.5, def_radius: 20, non_rotating: true) do
    collide :wall
    collide :void
    pass :player
    pass :enemy
  end

  material(:player_shot, density: 10, friction: 0.5, elasticity: 0.5, def_radius: 1) do
    collide_event :wall, ShotHitWall, first_only: true
    pass :void
    pass :body
    collide_event :enemy, ShotHitsEnemy, first_only: true
    pass :player
  end

  material(:enemy_shot, density: 10, friction: 0.5, elasticity: 0.5, def_radius: 1) do
    collide_event :wall, ShotHitWall, first_only: true
    pass :void
    pass :body
    pass :enemy
    collide_event :player, ShotHitsPlayer, first_only: true
  end

  material(:spirit_arrow, density: 1, friction: 0.5, elasticity: 0.5, def_radius: 1) do
    collide_event :wall, ShotHitWall, first_only: true
    pass :void
    collide_event :body, SpiritShotHitsBody, first_only: true
    collide_event :enemy, SpiritShotHitsEnemy, first_only: true
    pass :player
  end

  pass :enemy_shot, :player_shot
  pass :enemy_shot, :spirit_arrow
  pass :player_shot, :spirit_arrow

  material(:see_player, density: 1, friction: 0.5, elasticity: 0.5, def_radius: 1) do
    collide :wall
    collide :player
  end
  material(:see_enemy, density: 1, friction: 0.5, elasticity: 0.5, def_radius: 1) do
    collide :wall
    collide :enemy
  end
end
