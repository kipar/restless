include Engine

THE_SCREEN     = Sprite.new(-1)
NO_MUSIC       = Sound.new(-1)
DEFAULT_SHADER = DefaultShader.new(0)
ALL_SHADERS    = ShaderAllShaders.new(-1)

module RES
  module Gui
    Sound_off   = Sprite.new(0)
    Sound_on    = Sprite.new(1)
    Speaker     = Sprite.new(2)
    Speaker_off = Sprite.new(3)
  end

  module Skills
    Crossbow  = Sound.new(0)
    Crossbow3 = Sound.new(1)
    Curse     = Sound.new(2)
    Explode   = Sound.new(3)
    Melee     = Sound.new(4)
    Melee3    = Sound.new(5)
    Shot_fire = Sound.new(6)
    Sword     = Sound.new(7)
    Sword3    = Sound.new(8)
    Traphit   = Sound.new(9)
    Warp      = Sound.new(10)
  end

  Config        = RawResource.new(0)
  Determination = Sound.new(11)
  Font          = FontResource.new(0)
  Music         = Sound.new(12)
  Music_2       = Sound.new(13)
  Tilesheet     = TileMap.new(0)
  N_SPRITES     = 4
  N_TILEMAPS    = 1
end

class ShaderAllShaders < Shader
  uniform screen_size, vec2, 0
  uniform tex, sampler2D, 1
  attribute color, vec4, 0
  attribute pos, vec3, 1
  attribute texpos, vec3, 2
end
