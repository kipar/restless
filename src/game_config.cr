require "yaml"

CONFIG_PRESSED = KeysConfig.new
CONFIG_PRESSED[Engine::Key::Escape] = MainMenu.new

CONFIG_DOWN = KeysConfig.new

enum F
  Main
end

Fonts = {} of F => Engine::Font

def init_fonts
  Fonts[F::Main] = Font.new(RES::Font, char_size: 16, color: Color::BLACK)
end

class GameConfig
  include YAML::Serializable

  getter map_width : Int32
  getter map_height : Int32
  getter map_cell : Int32
  getter damping : Float64
  getter move_speed : Float64
  getter move_force : Float64
  getter n_enemies : Int32
  property seed : Int32
  getter move_target : Float64
  getter melee_force : Float64
  getter fixed_class : Int32
  getter easy_bonus : Float64
  getter hard_damage : Float64
  property difficulty : Int32
  getter balance_hp : Float64

  getter void_start : Float64
  getter void_expansion : Float64
  getter void_passes : Int32

  getter sound_distance : Float64
  getter min_sound : Float64
  getter enemy_sound : Float64

  getter menu_music : Int32
  getter battle_music : Int32

  getter pathfinding : Bool

  getter campaign : Array(Int32)

  @@instance : GameConfig?

  def self.instance
    @@instance.not_nil!
  end

  def self.init_config
    @@instance = GameConfig.from_yaml(RES::Config.as_string)
    @@instance.not_nil!.patch
    init_fonts
  end

  def patch
    if @seed < 0
      @seed = Random::Secure.next_int
      Engine.log("Level seed: #{@seed}")
    end
  end
end

def cfg
  GameConfig.instance
end

def fnt
  Fonts[F::Main]
end
