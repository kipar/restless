
использованных внешних ресурсов с указанием их авторства и источника.

Игра: Restless для конкурса "Бумага всё стерпит"
Автор: Андрей Коновод-Кац (kipar)

Краткая инструкция:
    Управляем персонажем умеющим переселяться в убитых врагов. На каждом уровне требуется добраться до выхода.
    WSAD (или стрелки) - движение
    левая и правая кнопка мыши - использование атак
    Q - выпустить стрелу духа (слабый урон, но переселяет игрока в тело убитого, можно стрелять в тела).
    Esc - выйти в меню

Основное меню:
    - Кампания состоит из N уровней, по которым ведется статистика наилучшего времени и наибольшего процента побежденных врагов
    - Случайный уровень отправляет на сгенерированный уровень.
    - Сложность переключает сложность
        - на низкой урон по игроку уменьшен вдвое
        - на средней урон нормальный
        - на высокой игрок теряет 2% максимального здоровья в секунду


Использованы внешние ресурсы (кроме входящих в условие конкурса):

Музыка:
	https://opengameart.org/content/boss-battle-music (Author: SubspaceAudio)
	https://opengameart.org/content/determination (Author: artisticdude)
	https://opengameart.org/content/determined-pursuit-epic-orchestra-loop (Author: Emma_MA)

Звуковые эффекты:
	https://opengameart.org/content/3-melee-sounds (Author: remaxim)
	https://opengameart.org/content/50-rpg-sound-effects (Author: Kenney)
	https://opengameart.org/content/crossbow-shot (Author: spookymodem)
	https://opengameart.org/content/spell-sounds-starter-pack (Author: p0ss)
    - 
